SizeOfFloat        = 4      ;1 FLOAT  = 4 BYTE
MaxCountOfMeasures = 0FFFFh ;1 COLUMN = 65535 (2^16 - 1) FLOAT
MaxCountOfChannels = 11111b ;1 COLUMN = not more, than 63 (2^5 - 1) COVARIANCE VALUE

.DATA
    matrix          REAL4 MaxCountOfChannels * MaxCountOfChannels DUP(0.0) ;(64 x 64) sqaure matrix
    averageVector   REAL4 MaxCountOfChannels                      DUP(0.0)
    tempVector      REAL4 MaxCountOfChannels                      DUP(0.0)

    matrixOriginPtr QWORD 0 ;Direct pointer to the original matrix
    countOfMeasures WORD  0
    countOfChannels BYTE  0

    i               BYTE  0 ;Column(i) index
    j               BYTE  0 ;Column(j) index

.CODE
Covariance PROC USES RBX RCX RBP RDI RSI
    MOV      QWORD PTR matrixOriginPtr, RCX
    MOV      WORD  PTR countOfMeasures, DX
    MOV      RAX, R8
    MOV      BYTE  PTR countOfChannels, AL
    CALL     Averages
    MOV      RSI, RCX                     ;Get pointer of the float matrix
    MOV      RDI, RCX

    XOR      RAX, RAX                     ;Set the whole akumulator register to 0 (needed only 1 time)
    XOR      RBX, RBX                     ;Set the whole base       register to 0 (needed only 1 time)
    XOR      RCX, RCX                     ;Set the whole counter    register to 0 (needed only 1 time)
    
    XORPS    XMM5, XMM5
    XOR      EDX, EDX
    MOV      DX, WORD PTR countOfMeasures
    CVTSI2SS XMM5, EDX                    ;ConVerT Scalar Integer TO Scalar Single-precision floating-point into XMM5 from EDX
Covariance_LoopStart:
                                          ;XORPS => XOR Packaged Single-precisoin floating-point
    XORPS    XMM4, XMM4                   ;Set the summary to 0.0f
    MOV      CX, WORD PTR countOfMeasures ;Get count of the float array

Covariance_CalcStart:                     ;Calculation inner loop start
    MOV      AL, [i]
    MOV      BL, SizeOfFloat
    MUL      BL
    LEA      RBP, averageVector
    ADD      RBP, RAX

    MOVSS    XMM2, REAL4 PTR [RBP]  ;Load averageVector[i] to XMM2
    MOVSS    XMM1, REAL4 PTR [RDI]  ;Load M[k,i] = M[DX - CX, i] to XMM1
    SUBSS    XMM2, XMM1             ;1st (i-th) subtract in the sum

    MOVSS    XMM3, XMM2

    MOV      AL, [j]                ;BL = SizeOfFloat
    MUL      BL
    LEA      RBP, averageVector
    ADD      RBP, RAX

    MOVSS    XMM2, REAL4 PTR [RBP]  ;Load averageVector[j] to XMM2
    MOVSS    XMM1, REAL4 PTR [RSI]  ;Load M[k,j] = M[DX - CX, j] to XMM1
    SUBSS    XMM2, XMM1             ;2nd (j-th) subtract in the sum

    MULSS    XMM3, XMM2             ;Multiply the 2 subtraction

    ADDSS    XMM4, XMM3             ;Add it to the summary

    ADD      RDI, SizeOfFloat
    ADD      RSI, SizeOfFloat
    LOOP     Covariance_CalcStart   ;Calculation inner loop end (CX--)

    DIVSS    XMM4, XMM5             ;Divide by the count of measures of the original matrix

    ;Target pointer: Matrix pointer + [(i*MaxCountOfChannels + j) * SizeOfFloat]
    MOV      AL, [i]
    MOV      CL, BYTE PTR MaxCountOfChannels    ;4: Size of channels
    MUL      CL
    ADD      AL, [j]
    MOV      CL, SizeOfFloat
    MUL      CL
    LEA      R8, matrix
    ADD      R8, RAX
    MOVSS    REAL4 PTR [R8], XMM4   ;Store done covariance value

    MOV      AL, [j]
    INC      AL
    MOV      [j], AL                ; j++
    MOV      RSI, QWORD PTR matrixOriginPtr
    MOV      CL, SizeOfFloat
    MUL      CL
    MOV      CX, WORD PTR MaxCountOfMeasures
    MUL      ECX
    ADD      RSI, RAX               ; RSI:= matrixOriginPtr + j * MaxCountOfMeasures * SizeOfFloat "To the top of actual column"
    
    XOR      EAX, EAX
    MOV      AL, [i]
    MOV      RDI, QWORD PTR matrixOriginPtr
    MOV      CL, SizeOfFloat
    MUL      CL
    MOV      CX, WORD PTR MaxCountOfMeasures
    MUL      ECX
    ADD      RDI, RAX               ; RDI:= matrixOriginPtr + i * MaxCountOfMeasures * SizeOfFloat "To the top of actual column"

    XOR      EAX, EAX
    MOV      AL, [j]
    CMP      AL, countOfChannels
    JB       Covariance_LoopStart
    
    XOR      AX, AX
    MOV      [j], AL                ; j:=0
    
    MOV      AL, [i]
    INC      AL
    MOV      [i], AL                ; i++
    MOV      RDI, QWORD PTR matrixOriginPtr
    MOV      CL, SizeOfFloat
    MUL      CL
    MOV      CX, WORD PTR MaxCountOfMeasures
    MUL      ECX
    ADD      RDI, RAX               ; RDI:= matrixOriginPtr + i * MaxCountOfMeasures * SizeOfFloat "To the top of actual column"
    MOV      RSI, QWORD PTR matrixOriginPtr

    XOR      EAX, EAX
    MOV      AL, [i]
    CMP      AL, countOfChannels
    JB       Covariance_LoopStart

    LEA      RAX, matrix
    RET
Covariance ENDP

CopyAverageVector PROC USES RAX R8
                                    ;MOVAPS => MOVe Alingned Packaged Single-precision floating-point
    LEA    RAX, averageVector       ;Get the pointer of local averageVector
    MOVAPS XMM0, REAL4 PTR [R8]    ;Load average vector to XMM0
    MOVAPS REAL4 PTR [RAX], XMM0    ;Save average vector local averageVector
    RET
CopyAverageVector ENDP

;-----------------------------------------------------------------------------------------------------------------------------
; AVERAGE CALCULATION
;-----------------------------------------------------------------------------------------------------------------------------

Averages PROC USES RAX RBX RCX RBP
	MOV  RBX, QWORD PTR matrixOriginPtr         ;Get pointer of the float array to base register

    XOR  RAX, RAX
    MOV  AX, WORD PTR countOfMeasures          ;Get count of measures the float array to counter regiszter

    XOR RCX, RCX
    MOV CL, countOfChannels

    LEA RBP, averageVector

Averages_Element:
    CALL CalcAverage
    ADD RBX, SizeOfFloat * MaxCountOfMeasures
    ADD RBP, SizeOfFloat
    LOOP Averages_Element

    RET
Averages ENDP

CalcAverage PROC USES RBX RCX RDX
    XORPS XMM0 , XMM0
    XORPS XMM1 , XMM1
    XORPS XMM2 , XMM2
    XORPS XMM3 , XMM3
    XORPS XMM4 , XMM4
    XORPS XMM5 , XMM5
    XORPS XMM6 , XMM6
    XORPS XMM7 , XMM7
    XORPS XMM8 , XMM8
    XORPS XMM9 , XMM9
    XORPS XMM10, XMM10
    XORPS XMM11, XMM11
    XORPS XMM12, XMM12
    XORPS XMM13, XMM13
    XORPS XMM14, XMM14
    XORPS XMM15, XMM15
    
    XOR RCX, RCX
    MOV CX, WORD PTR countOfMeasures
    MOV RDX, RCX

CalcAverage_AveragingStart:

    ADDSS XMM0, REAL4 PTR [RBX]
    ADD   RBX, SizeOfFloat
    DEC   CX
    JZ    CalcAverage_AveragingEnd

    ADDSS XMM1, REAL4 PTR [RBX]
    ADD   RBX, SizeOfFloat
    DEC   CX
    JZ    CalcAverage_AveragingEnd

    ADDSS XMM2, REAL4 PTR [RBX]
    ADD   RBX, SizeOfFloat
    DEC   CX
    JZ    CalcAverage_AveragingEnd

    ADDSS XMM3, REAL4 PTR [RBX]
    ADD   RBX, SizeOfFloat
    DEC   CX
    JZ    CalcAverage_AveragingEnd

    ADDSS XMM4, REAL4 PTR [RBX]
    ADD   RBX, SizeOfFloat
    DEC   CX
    JZ    CalcAverage_AveragingEnd

    ADDSS XMM5, REAL4 PTR [RBX]
    ADD   RBX, SizeOfFloat
    DEC   CX
    JZ    CalcAverage_AveragingEnd

    ADDSS XMM6, REAL4 PTR [RBX]
    ADD   RBX, SizeOfFloat
    DEC   CX
    JZ    CalcAverage_AveragingEnd

    ADDSS XMM7, REAL4 PTR [RBX]
    ADD   RBX, SizeOfFloat
    DEC   CX
    JZ    CalcAverage_AveragingEnd

    ADDSS XMM8, REAL4 PTR [RBX]
    ADD   RBX, SizeOfFloat
    DEC   CX
    JZ    CalcAverage_AveragingEnd

    ADDSS XMM9, REAL4 PTR [RBX]
    ADD   RBX, SizeOfFloat
    DEC   CX
    JZ    CalcAverage_AveragingEnd

    ADDSS XMM10, REAL4 PTR [RBX]
    ADD   RBX, SizeOfFloat
    DEC   CX
    JZ    CalcAverage_AveragingEnd

    ADDSS XMM11, REAL4 PTR [RBX]
    ADD   RBX, SizeOfFloat
    DEC   CX
    JZ    CalcAverage_AveragingEnd

    ADDSS XMM12, REAL4 PTR [RBX]
    ADD   RBX, SizeOfFloat
    DEC   CX
    JZ    CalcAverage_AveragingEnd

    ADDSS XMM13, REAL4 PTR [RBX]
    ADD   RBX, SizeOfFloat
    DEC   CX
    JZ    CalcAverage_AveragingEnd

    ADDSS XMM14, REAL4 PTR [RBX]
    ADD   RBX, SizeOfFloat
    DEC   CX
    JZ    CalcAverage_AveragingEnd

    ADDSS XMM15, REAL4 PTR [RBX]
    ADD   RBX, SizeOfFloat
    DEC   CX

    JNZ  CalcAverage_AveragingStart
CalcAverage_AveragingEnd:

    ADDSS XMM0 , XMM1 
    ADDSS XMM2 , XMM3 
    ADDSS XMM4 , XMM5 
    ADDSS XMM6 , XMM7 
    ADDSS XMM8 , XMM9 
    ADDSS XMM10, XMM11
    ADDSS XMM12, XMM13
    ADDSS XMM14, XMM15

    ADDSS XMM0 , XMM2
    ADDSS XMM4 , XMM6
    ADDSS XMM8 , XMM10
    ADDSS XMM12, XMM14

    ADDSS XMM0, XMM4
    ADDSS XMM8, XMM12

    ADDSS XMM0, XMM8

    XORPS    XMM1, XMM1
    CVTSI2SS XMM1, RDX
    DIVSS    XMM0, XMM1
    
    MOVSS REAL4 PTR [RBP], XMM0

    RET
CalcAverage ENDP
END