#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>

using namespace std;

#define MaxCountOfChannel 31    //2^5 - 1  -> 0000 0000  0001 1111
#define MaxCountOfMeasure 65535 //2^16 - 1 -> 1111 1111  1111 1111

enum ELogLevel
{
	Nothig,
	Addresses,
	Everithig
};
char*      g_cPath;
ELogLevel* g_eLogLevel;

float        g_matrix[MaxCountOfChannel][MaxCountOfMeasure];
float        g_matrixCov[MaxCountOfChannel][MaxCountOfChannel];
unsigned int g_arrTime[MaxCountOfMeasure];

char           g_cCountOfChannel;
unsigned short g_sCountOfMeasure;

int main(int argCount, char* args[]);

void Init(int argCount, char* args[]);
bool IsSameString(char* pcLeft, char* pcRight);
int LoadFile();

extern "C" float* Covariance(float* matrix, unsigned short CountOfMeasure, char CountOfChannel);