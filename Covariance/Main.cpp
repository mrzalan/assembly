﻿#include "Main.h"

int main(int argCount, char* args[])
{
    Init(argCount, args);
	LoadFile();

    if (*g_eLogLevel >= ELogLevel::Addresses)
    {
        for (char i = 0; i < g_cCountOfChannel; i++)
        {
            cout << "Memory address of column " << i << " of channel matrix: " << g_matrix[i] << endl;
        }
        cout << endl;
    }

    float* matrix;
    matrix = Covariance((float*)g_matrix, g_sCountOfMeasure, g_cCountOfChannel);
    for (unsigned short i = 0; i < g_cCountOfChannel; i++)
    {
        for (unsigned short j = 0; j < g_cCountOfChannel; j++)
        {
            float* floatPtr = matrix + (i * MaxCountOfChannel + j);
            g_matrixCov[i][j] = *floatPtr;
            printf("%-15f", g_matrixCov[i][j]);
        }
        cout << endl << endl;
    }

    return 0;
}

void Init(int argCount, char* args[])
{
    setlocale(LC_ALL, "");

    g_cPath     = nullptr;
    g_eLogLevel = nullptr;

    //Paramether processing
    for (int i = 1; i < argCount; i++)
    {
        if (IsSameString(args[i], (char*)"-p") ||
            IsSameString(args[i], (char*)"-path"))
        {
            i++;
            if (i >= argCount)
            {
                cout << "Switch '-path' is not complet." << endl;
                exit(-1);
            }
            g_cPath = args[i];
        }

        if (IsSameString(args[i], (char*)"-l") ||
            IsSameString(args[i], (char*)"-log"))
        {
            i++;
            if (i >= argCount)
            {
                cout << "Switch '-log' is not complet." << endl;
                exit(-1);
            }
            /**/ if (IsSameString(args[i], (char*)"nothig") ||
                    IsSameString(args[i], (char*)"n")      )
            {
                g_eLogLevel = new ELogLevel(ELogLevel::Nothig);
            }
            else if(IsSameString(args[i], (char*)"addresses") ||
                    IsSameString(args[i], (char*)"a")     )
            {
                g_eLogLevel = new ELogLevel(ELogLevel::Addresses);
            }
            else if(IsSameString(args[i], (char*)"everythig") ||
                    IsSameString(args[i], (char*)"e")      )
            {
                g_eLogLevel = new ELogLevel(ELogLevel::Everithig);
            }
            else
            {
                cout << "Unabled value to switch '-log'." <<endl; 
                cout << "'everythig', 'e', 'addresses', 'a', 'nothing', and 'n' are enabled." << endl;
                cout << "Default value has been set." << endl;
                cout << endl;
            }
        }
    }

    //Set default paramethers in case they're not set
    if (g_cPath == nullptr)
        g_cPath = (char*)"Minta.csv";

    if (g_eLogLevel == nullptr)
        g_eLogLevel = new ELogLevel(ELogLevel::Nothig);

    //Log paramether values to console
    cout << "Console log:  ";
    switch (*g_eLogLevel)
    {
    case ELogLevel::Nothig:
        cout << "Nothig" << endl;
        break;
    case ELogLevel::Addresses:
        cout << "Addresses" << endl;
        break;
    default:
        cout << "Everything" << endl;
        break;
    }
    
    if (*g_eLogLevel >= ELogLevel::Addresses)
    {
        cout << "File path: " << g_cPath << endl;
    }
    cout << endl;

    for (int i = 0; i < MaxCountOfChannel; i++)
    {
        for (int j = 0; j < MaxCountOfChannel; j++)
        {
            g_matrixCov[i][j] = 0.0;
        }
    }
}

int LoadFile()
{
	fstream fin;
	fin.open(g_cPath, ios::in);
    g_cCountOfChannel = -1; //No need to add column time as a channel

	vector<string> row;
	string line, word, temp;

    getline(fin, line);
    istringstream iss(line);
    while (getline(iss, word, ';'))
    {
        row.push_back(word);
        g_cCountOfChannel++;
    }

    if (*g_eLogLevel == ELogLevel::Everithig)
    {
        printf("%-10s", (char*)(&row[0]));
        for (char i = 1; i < g_cCountOfChannel; i++)
        {
            printf("%-15s", (char*)(&row[i]));
        }
        cout << endl;
    }

    while (getline(fin, line))
    {
        row.clear();
        istringstream iss(line);
        while (getline(iss, word, ';'))
        {
            row.push_back(word);
        }

        g_arrTime[g_sCountOfMeasure] = (unsigned int)stoi(row[0]);
        for (char i = 0; i < g_cCountOfChannel; i++)
        {
            g_matrix[i][g_sCountOfMeasure] = stof(row[i+1]);
        }

        if (*g_eLogLevel == ELogLevel::Everithig)
        {
            printf("%-10i", g_arrTime[g_sCountOfMeasure]);
            for (char i = 1; i < g_cCountOfChannel; i++)
            {
                printf("%-15f", g_matrix[i - 1][g_sCountOfMeasure]);
            }
            cout << endl;
        }

        g_sCountOfMeasure++;
    }
    return 0;
}

bool IsSameString(char* pcLeft, char* pcRight)
{
    do
    {
        if (*pcLeft != *pcRight)
            return false;

        pcLeft++;
        pcRight++;
    } while (*pcLeft || *pcRight);

    return *pcLeft == *pcRight;
}